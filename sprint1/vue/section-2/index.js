new Vue({
    el: '#app',
    data: {
        pageTitle: 'Section 2: Use VueJS to interact with the DOM',
        googleLink: 'https://www.google.com',
        linkTitle: 'Here is a link',
        finishedLink: '<a href="http://google.com">Google</a>',
        counter: 0,
        x: 0,
        y: 0,
    },
    methods: {
        setPageTitle: function(){
            this.pageTitle = "This title was set in a function, it doesnt change the original value because we added the v-once directive to the first title.";
            return this.pageTitle;
        },
        increase: function(){
            this.counter++;
        },
        updateCoordinates: function(event){ //when hovering over the section, the coordinates are updated.
            this.x = event.clientX;
            this.y = event.clientY;
        },
        dummy: function(event){
            event.stopPropagation(); //stopping updating when we hover over the dead spot
        },
        alertMe: function(){
            alert('You pressed Enter!');
        }
    }
})