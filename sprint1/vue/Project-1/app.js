new Vue({
    el: '#app',
    data: {
        gameIsRunning: false,
        playerHealth: 100,
        monsterHealth: 100,
        turns: []
    },
    methods: {
        startGame: function(){
            //start the game
            this.gameIsRunning = true;

            //set initial health values
            this.playerHealth = 100;
            this.monsterHealth = 100;
            this.turns = [];
        },
        attack: function(){
            var damage = this.generateDamage(3, 10);
            this.monsterHealth -= damage;
            this.turns.unshift({
                isPlayer: true,
                text: 'Player hits Monster for ' + damage
            });//adds to first
            if(this.checkWin()){
                return;
            }
            this.monsterAttack();
        },
        specialAttack: function(){
            var damage = this.generateDamage(10, 20);
            this.monsterHealth -= damage;
            this.turns.unshift({
                isPlayer: true,
                text: 'Player hits Monster for ' + damage
            });
            if(this.checkWin()){
                return;
            }
            this.monsterAttack();
        },
        monsterAttack: function(){
            var damage = this.generateDamage(5, 12);
            this.playerHealth -= damage;
            this.checkWin;
            this.turns.unshift({
                isPlayer: false,
                text: 'Monster hits Player for ' + damage
            });
        },
        heal: function(){
            if(this.playerHealth <= 90){
                this.playerHealth += 10;
            }else{
                this.playerHealth = 100;
            }
            this.turns.unshift({
                isPlayer: true,
                text: 'Player healed for ' + 10
            });
            
            this.monsterAttack();
        },
        giveUp: function(){
            this.gameIsRunning = false;
        },
        generateDamage: function(min, max){
            return Math.max(Math.floor(Math.random() * max) + 1, min);
        },
        checkWin: function() {
            if (this.monsterHealth <= 0) {
                if (confirm('You won! New Game?')) {
                    this.startGame();
                } else {
                    this.gameIsRunning = false;
                }
                return true;
            } else if (this.playerHealth <= 0) {
                if (confirm('You lost! New Game?')) {
                    this.startGame();
                } else {
                    this.gameIsRunning = false;
                }
                return true;
            }
            return false;
        }
    },
    computed: {
        
    },
    watches: {

    }
});

/*
    Features:

    Health Bars:
    - Health of You
    - Health of Monstor
    - Style should be conditionally changed depending on the health

    Start New Game Button 
    - Shows and Hides the Game controls
    - Sets default values

    Attack

    Special Attack

    Heal

    Give Up
    

*/