new Vue({
        el: '#exercise',
        data: {
            value: ''
        },
        methods: {
            alert: function(){
                alert('You pressed the button');
            },
            onKeyDown: function(event){
                this.value = event.target.value;
            }
        }
    });