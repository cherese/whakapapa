new Vue({
        el: '#exercise',
        data: {
            value: 0,
        },
        //when a value depends on another, it indicates we want to use a computed property
        computed: {
            result: function(){
                return this.value == 37 ? 'done' : 'not there yet';
            }
        },
        watch: {
            result: function(){
                var vm = this;
                setTimeout(function(){ //after 5 seconds it will reset the value property
                    vm.value = 0;
                }, 5000);
            }
        }
    });