


//DOM SELECTION AND MANIPULATION
//d3 methods to select dom elements 
d3.select(); //returns the first element matching the criteria
d3.selectAll(); //returns all elements matching the criteria

d3.select('h1').style('color', 'red') //selects the first h1 element and sets the style for it
.attr('class', 'heading') //sets the class for this h1 element to heading
.text('Updated h1 tag'); //sets the text property of the h1 element

d3.select('body').append('p').text('First Paragraph');
d3.select('body').append('p').text('Second Paragraph');
d3.select('body').append('p').text('Third Paragraph');

d3.selectAll('p').style('color', 'blue');

//DATA LOADING AND BINDING

var dataset = [1, 2, 3, 4, 5];
d3.select('body')
.selectAll('h2')
.data(dataset) 
.enter() //takes data items from dataset one by one
.append('p') //for each data item from dataset, append a paragraph tag
.text('D3 is awesome!!'); //and set the text to this

//CREATING A SIMPLE BAR CHART
var dataset2 = [80, 100, 56, 120, 180, 30, 40, 120, 160];

var svgWidth = 500, svgHeight = 300, barPadding = 5;
var barWidth = (svgWidth / dataset.length);

var svg = d3.select('svg')
    .attr('width', svgWidth)
    .attr('height', svgHeight);

var barChart = svg.selectAll('rect')
    .data(dataset2)
    .enter()
    .append('rect')
    .attr('y', function(d) {
        return svgHeight - d
    })
    .attr('height', function(d){
        return d;
    })
    .attr('width', barWidth - barPadding)
    .attr('transform', function(d, i){
        var translate = [barWidth * i, 0];
        return 'translate('+ translate + ')';
    })
    .attr('fill', '#A1A1A1');

var text = svg.selectAll('text')
    .data(dataset2)
    .enter()
    .append('text')
    .text(function(d){
        return d;
    })
    .attr('y', function(d, i){
        return svgHeight - d - 2;
    })
    .attr('x', function(d, i){
        return barWidth * i;
    })
    .attr('fill', '#A64C38');

