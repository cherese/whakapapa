# Whakapapa

```
.
├──sprint1
|   ├── vue
|   |    ├──Exercise 1
|   |    |   ├──how-to-use.txt
|   |    |   ├──index.html
|   |    |   └──index.js
|   |    ├──Exercise 2
|   |    |   ├──how-to-use.txt
|   |    |   ├──index.html
|   |    |   └──app.js
|   |    ├──Exercise 3
|   |    |   ├──how-to-use.txt
|   |    |   ├──index.html
|   |    |   └──app.js
|   |    ├──Practise 1
|   |    |   ├──index.html
|   |    |   └──index.js
|   |    ├──section-2
|   |    |   ├──index.html
|   |    |   └──index.js
|   ├── vuex
|   └── d3
└──README.md

```